from datetime import datetime
from rest_framework.views import APIView
from facts.serializers import (
    NewsSerializer, CreateNewSerializer, CreateEvidanceSerializer,
    BindEvidenceSerializer, BindNewsToNewsSerializer
)
from facts.models import News, Evidence
from rest_framework.response import Response
from rest_framework import status


class NewsView(APIView):

    def get(self, request):
        news = News.get_all_news()
        serializer = NewsSerializer(news, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        data = CreateNewSerializer(data=request.data)
        if not data.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST)

        validated_data = data.validated_data
        News.create_news(validated_data['title'], validated_data['img_url'])
        return Response(status=status.HTTP_201_CREATED)


class EvidenceView(APIView):

    def post(self, request):
        data = CreateEvidanceSerializer(data=request.data)
        if not data.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST)

        validated_data = data.validated_data
        created_at = datetime.fromtimestamp(validated_data['created_at'])
        validated_data.update({
            'created_at': created_at
        })
        evidance_instance = Evidence.create(validated_data)
        return Response(
            {"id": evidance_instance.id},
            status=status.HTTP_201_CREATED
        )


class BindEvidenceToNewsView(APIView):

    def post(self, request):
        data = BindEvidenceSerializer(data=request.data)
        if not data.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST)
        validated_data = data.validated_data

        news = News.get_by_id(validated_data['news_id'])
        evidance = Evidence.get_by_id(validated_data['evidence_id'])
        if not news or not evidance:
            return Response({'Error': 'News Not found'}, status=status.HTTP_400_BAD_REQUEST)
        news.bind_evidence(evidance)
        return Response(status=status.HTTP_200_OK)


class BindNewsToNewsAPI(APIView):

    def post(self, request):
        data = BindNewsToNewsSerializer(data=request.data)
        if not data.is_valid():
            return Response(status=status.HTTP_400_BAD_REQUEST)

        validated_data = data.validated_data
        news = News.get_by_id(validated_data['news_id'])
        added_news = News.get_by_id(validated_data['add_news_id'])
        if not news or not added_news:
            return Response({'Error': "News not found"}, status=status.HTTP_400_BAD_REQUEST)
        news.bind_news(added_news)
        return Response(status=status.HTTP_200_OK)

class NewsDetailedView(APIView):
    def get(self, request, news_id=None):
        return Response({'some': 'new'}, status=status.HTTP_200_OK)
