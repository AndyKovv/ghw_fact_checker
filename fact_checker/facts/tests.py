
from datetime import datetime

from django.test import TestCase
from rest_framework.test import APITestCase, APIRequestFactory
from facts.models import News, Evidence
from facts.views import (
    NewsView, EvidenceView,
    BindEvidenceToNewsView, NewsDetailedView,
    BindNewsToNewsAPI
)

# interface NewsItem {
#   id: number;
#   createdAt: string;
#   title: string;
#   imgUrl: string;
# }


def create_news(title, img_url):
    news = News.create_news(
        title,
        img_url
    )
    return news


def create_evidence(img_url, coords, timestamp):
    data = {
        'img_url': img_url,
        'coords': coords,
        'created_at': datetime.fromtimestamp(timestamp)
    }
    evidence = Evidence.create(data)
    return evidence


def create_news_to_evidence_bind(news_id, evidence):
    news = News.get_by_id(news_id)
    news.bind_evidence(evidence)
    return news


def create_news_to_news_bind(news_id, add_news):
    news = News.get_by_id(news_id)
    news.bind_news(add_news)
    return news


class NewsTestAPICase(APITestCase):

    def setUp(self):
        title_one = "some_news"
        image_url = "/some/image/url"
        self.news_one = create_news(title_one, image_url)
        self.factory = APIRequestFactory()

    def test_should_get_news_from_api(self):
        news_view = NewsView.as_view()
        request = self.factory.get('/api/v1/news/')
        response = news_view(request)
        self.assertEqual(response.status_code, 200)
        response.render()

        data = response.data

        self.assertEqual(len(data), 1)

        self.assertEqual(data[0]['id'], self.news_one.id)
        self.assertIsNotNone(data[0]['created_at'])
        self.assertEqual(data[0]['title'], self.news_one.title)
        self.assertEqual(data[0]['img_url'], self.news_one.img_url)

    def test_should_add_news_to_database(self):
        data = {
            'title': "My shiney title",
            'img_url': 'http://my-shiney.url/com.jpg'
        }
        news_view = NewsView.as_view()
        request = self.factory.post('/api/v1/news/', data)
        response = news_view(request)
        self.assertEqual(response.status_code, 201)

        request = self.factory.get('/api/v1/news/')
        response = news_view(request)
        self.assertEqual(response.status_code, 200)
        response.render()

        data = response.data

        self.assertEqual(len(data), 2)


class EvidenceTestAPICase(APITestCase):

    def setUp(self):
        self.factory = APIRequestFactory()

    def test_should_add_new_evidance(self):

        data = {
            'img_url': "http://some-url.com/image.jpg",
            'coords': '50.448787, 30.442248',
            'created_at': 1543063131
        }

        evidance_view = EvidenceView.as_view()
        request = self.factory.post('/api/v1/evidence/', data)
        response = evidance_view(request)
        response.render()

        self.assertEqual(response.status_code, 201)
        data = response.data
        evidence = Evidence.get_all()
        self.assertEqual(len(evidence), 1)

        second_evidence = {
            'img_url': "http://some-url.com/image.jpg",
            'coords': '50.448787, 30.442248',
            'created_at': 1543063131
        }
        request = self.factory.post("/api/v1/evidence/", second_evidence)
        response = evidance_view(request)
        self.assertEqual(response.status_code, 201)

        evidence = Evidence.get_all()
        self.assertEqual(len(evidence), 2)


class BindEvidenceToNewsAPITests(APITestCase):

    def setUp(self):
        news_title = "NewsTitle"
        img_url = "http://gogl.com/some.jpg"
        self.news = create_news(news_title, img_url)

        evidence_url = "http://some-url.com/image.jpg"
        coords = '50.448787, 30.442248'
        created_at = 1543063131
        self.evidence = create_evidence(evidence_url, coords, created_at)
        self.second_news = create_news("SomeTitle", "http://some-title.com/news.jpg")
        self.factory = APIRequestFactory()

    def test_should_bind_evidance_to_news(self):
        view = BindEvidenceToNewsView.as_view()
        data = {
            'news_id': self.news.id,
            'evidence_id': self.evidence.id
        }
        request = self.factory.post('/api/v1/evidence-to-news/', data)
        response = view(request)
        self.assertEqual(response.status_code, 200)

        news = News.get_news_by_id(self.news.id)
        self.assertEqual(len(news.evidences.all()), 1)

    def test_shoudl_add_news_to_news(self):
        view = BindNewsToNewsAPI.as_view()
        data = {
            'news_id': self.news.id,
            'add_news_id': self.second_news.id
        }
        request = self.factory.post('/api/v1/news-to-news', data)
        response = view(request)
        self.assertEqual(response.status_code, 200)

        news = News.get_by_id(self.news.id)
        self.assertEqual(len(news.news_list.all()), 1)

    def test_should_fetch_news_by_id_with_evidences(self):
        create_news_to_evidence_bind(
            self.news.id, self.evidence
        )
        created_news = create_news_to_news_bind(
            self.news.id, self.second_news
        )

        view = NewsDetailedView.as_view()
        request = self.factory.get('/api/v1/news/{}'.format(self.news.id))
        response = view(request, news_id=self.news.id)

        self.assertEqual(response.status_code, 200)
        response.render()

        data = response.data

        self.assertEqual(data['id'], self.news.id)
        self.assertEqual(data['title'], self.news.title)
        self.assertEqual(data['img_url'], self.news.img_url)
        self.assertEqual(len(data['evidences']), 1)
        self.assertEqual(len(data['news_list']), 1)
