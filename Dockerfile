FROM python:3.6.1-alpine

RUN apk update \
  && apk add \
    build-base \
    libpq

RUN mkdir /usr/src/fact_checker
WORKDIR /usr/src/fact_checker
COPY ./requirements.txt .
RUN pip install --upgrade pip
RUN pip install gunicorn
RUN pip install -r requirements.txt

COPY . .
