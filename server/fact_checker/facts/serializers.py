
from rest_framework import serializers


class NewsSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.CharField()
    img_url = serializers.CharField()
    created_at = serializers.DateTimeField()


class CreateNewSerializer(serializers.Serializer):
    title = serializers.CharField(required=True)
    img_url = serializers.URLField(required=True)


class CreateEvidanceSerializer(serializers.Serializer):
    img_url = serializers.URLField()
    coords = serializers.CharField()
    created_at = serializers.IntegerField()


class BindEvidenceSerializer(serializers.Serializer):
    news_id = serializers.IntegerField(required=True)
    evidence_id = serializers.IntegerField(required=True)


class BindNewsToNewsSerializer(serializers.Serializer):
    news_id = serializers.IntegerField(required=True)
    add_news_id = serializers.IntegerField(required=True)
