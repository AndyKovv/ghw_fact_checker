from django.db import models


class Evidence(models.Model):
    img_url = models.URLField()
    coords = models.CharField(max_length=255)
    created_at = models.DateTimeField()

    @classmethod
    def create(cls, fields):
        return cls.objects.create(**fields)

    @classmethod
    def get_all(cls):
        return cls.objects.all()

    @classmethod
    def get_by_id(cls, evidence_id):
        return cls.objects.filter(id=evidence_id).last()


class News(models.Model):
    title = models.CharField(max_length=255)
    img_url = models.URLField()
    created_at = models.DateTimeField(auto_now_add=True)
    evidences = models.ManyToManyField(Evidence)
    news_list = models.ManyToManyField("self")

    @classmethod
    def create_news(cls, title, img_url):
        # Method should create news in database
        return cls.objects.create(title=title, img_url=img_url)

    @classmethod
    def get_all_news(cls):
        return cls.objects.all()

    @classmethod
    def get_news_by_id(cls, news_id):
        return cls.objects.get(id=news_id)

    @classmethod
    def get_by_id(cls, news_id):
        return cls.objects.filter(id=news_id).last()

    def bind_evidence(self, evidence):
        self.evidences.add(evidence)
        return self

    def bind_news(self, news):
        self.news_list.add(news)
        return self
