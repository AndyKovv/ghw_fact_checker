#!/bin/bash

WORK_DIR=/usr/src/fact_checker/fact_checker
USER=root
GROUP=root
PORT=80
NAME="fact_checker"
cd $WORK_DIR

# At this point we add migration
python manage.py migrate --noinput

# At this point we add collect static
python manage.py collectstatic --noinput
exec gunicorn fact_checker.wsgi:application \
  --name $NAME \
  --workers 5 \
  --threads 5 \
  --max-requests 800 \
  --user=$USER --group=$GROUP \
  --bind=0.0.0.0:$PORT \
  --log-level=debug \
  --log-file=/var/log/fact_checker.log

