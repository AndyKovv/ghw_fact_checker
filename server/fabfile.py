from fabric.api import env, run, prefix
from fabric.contrib.project import rsync_project

env.roledefs = {
    'production': ['root@188.166.167.151'],
}

RSYNC_EXCLUDES = ['local_settings.py', 'celerybeat-schedule.db', '.git', '*.pyc', 'htmlcov', '__pycache__', '.idea', 'locale', 'node_modules', 'db.sqlite3', '.env', 'web']


def rsync():
    rsync_project(remote_dir='/opt', exclude=RSYNC_EXCLUDES, delete=True)


def run_compose():
    run('docker-compose -f /opt/server/docker-compose.yml up -d')


def deploy():
    rsync()
    run_compose()

