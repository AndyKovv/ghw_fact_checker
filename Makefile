run:
	python fact_checker/manage.py runserver

deploy_production:
	fab -R production deploy

watch_test:
	@ if [[ ! -z "${test}" ]]; then \
		find . -name '*.py' | entr python ./fact_checker/manage.py test ${test} --noinput; \
	else \
		find . -name '*.py' | entr python ./fact_checker/manage.py test --pattern="tests_*.py" --noinput; \
	fi